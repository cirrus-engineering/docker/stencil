FROM ubuntu:bionic

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Update, and install base dependancies
RUN apt-get update -qq
RUN apt-get install -y -qq git curl python python-pip build-essential apt-transport-https

# Add virtualenv with pip
RUN pip install virtualenv

# Create NVM Env's
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 7.9.0

# Download NVM, then install specific Node version from Env's
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.28.0/install.sh | bash \
	&& source $NVM_DIR/nvm.sh \
	&& nvm install $NODE_VERSION \
	&& nvm alias default $NODE_VERSION \
	&& nvm use default
	
# Set Node path Env
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/v$NODE_VERSION/bin:$PATH

# Install Stencil CLI
RUN source $NVM_DIR/nvm.sh \
	&& npm install -g @bigcommerce/stencil-cli --loglevel=error
	
# Install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn -y -qq

RUN mkdir -p /opt/stencil

WORKDIR "/opt/stencil"
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]