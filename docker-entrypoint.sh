#!/bin/bash
set -e

source $NVM_DIR/nvm.sh

if [ "$1" == "bash" ]; then
	bash
elif [ "$1" == "start" ]; then
	if [ ! -e "/opt/stencil/package.json" ]; then
	    if [ ! "$(ls -A /opt/stencil)" ]; then
		    git clone git://github.com/bigcommerce/stencil.git /opt/stencil
	    else
	      echo "/opt/stencil must either be empty, or hold a valid stencil theme"
      fi
	fi

	if [ ! -e "/opt/stencil/node_modules" ]; then
		yarn
	fi

	if [ ! -e "/opt/stencil/.stencil" ]; then
		if [ -e "/etc/stencil/.stencil" ]; then
			cp -r /etc/stencil/.stencil /opt/stencil/
		else
			stencil init
		fi
	fi

	stencil start
else
	exec "$@"
fi