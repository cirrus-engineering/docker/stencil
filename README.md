# Stencil CLI Docker image

## About

This is a repository to create a Docker image with the BigCommerce Stencil CLI and all dependancies installed.
This saves a lot of download/install time, and space on your local machine.

### Login

Login to the Gitlab Docker registry, with `docker login registry.gitlab.com`.
You can then pull/push images from this repository.

### Pull

Pull the image with the below command:

```bash
docker pull registry.gitlab.com/cirrus-engineering/docker/stencil:latest
```

### Run

Make sure you are in the folder of the theme to be developed, and then run using the below command:

```bash
docker run --rm -v ${PWD}:/opt/stencil -p 3000:3000 -it registry.gitlab.com/cirrus-engineering/docker/stencil
```

This will run a few commands; If the current folder is empty, it will download the default Cornerstone theme from Github, then install dependancies with Yarn. If there is a theme in the folder, it will also install dependancies.
Then it will run `stencil init` to create a `.stencil` file, at this point it will then ask for the store details etc. If the folder already has a `.stencil` file, it will skip this. 

On subsequent runs, it will automatically detect if the Node dependancies are already installed - if so, it will skip this part. It will also detect the `.stencil` conf file, and will not try to reinit.

> `3000` is the default port stencil server starts on, this can be changed in the .stencil if needed.

#### Notes:

- **The container destroys itself after every exit** *(it's designed to be ephemeral with you maintaining your own stencil theme files)*
- You don't have to mount `.stencil`
- You don't have to mount a theme folder if you don't plan on editing files